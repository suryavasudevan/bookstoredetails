var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var path = require('path');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/views'));
//Store all HTML files in view folder

Genre = require('./models/genre.js');
Book = require('./models/book.js');



// connect to mongoose
mongoose.connect('mongodb://localhost/bookstore');
var db = mongoose.connection;


app.get('/', function(req,res){
    res.send('Please use /books or /genres');

});


app.get('/genres', function(req, res){
  Genre.getGenres(function(err, genres){
     if(err){
         throw err;
     }
    res.json(genres);    
    });
});

app.get('/genres/addgenre', function(req, res){
    res.sendFile(path.join(__dirname+'/views/addgenre.html'));
});
 
 app.get('/books/addBook', function(req, res){
    res.sendFile(path.join(__dirname+'/views/addbook.html'));
});

app.get('/books', function(req, res){
    Book.getBooks(function(err, books){
     if(err){
         throw err;
     }
    res.json(books);    
    });

});

app.get('/books/:_id', function(req, res){
    Book.getBookById(req.params._id,function(err, book){
     if(err){
         throw err;
     }
    res.json(book);    
     });
});

app.post('/genres/addgenre', function(req, res){
       new Genre({
        name      : req.body.name,
       create_date: req.body.date             
    }).save(function(err, name){
        if(err) {
            res.json(err);
        }
        res.send('Sucessfully inserted');
    });
}); 
// add genres using json files
   /* var genre = req.body;
    Genre.postGenre(genre, function(err, genre){
        if(err){
            throw err;
             }
             res.json(genre);
    });*/

app.post('/books/addBook', function(req, res){
   new Book({
        Title      : req.body.title,
        genre      :req.body.genre,
        description:req.body.desp,
        author     :req.body.author             
    }).save(function(err, name){
        if(err) {
            res.json(err);
        }
        res.send('Sucessfully inserted');
    });
});
// add books using json files
/*    var book = req.body;
    Book.postBook(book, function(err, book){
        if(err){
            throw err;
             }
             res.json(book);
    });*/



app.listen(7000);
console.log('Running successfully on port 7000');
