var mongoose = require('mongoose');

// Genre Schema
var bookSchema = mongoose.Schema({
    Title:{
        type: String,
        required: true
    },
     genre:{
        type: String,
        required: true
    },
     description:{
        type: String        
    },
      author:{
        type: String,
        required: true
    }       
});

var Book = module.exports = mongoose.model('Book', bookSchema);

// Get Books
module.exports.getBooks = function(callback,limit){
   Book.find(callback).limit(limit);
}

// Get Book by id
module.exports.getBookById = function(id,callback){
   Book.findById(id , callback);
}

// Post Books
module.exports.postBook = function(book,callback){
   Book.create(book,callback);
}
