This is a simple project retrieve and store data in a mongodb.
viewing results in local server.

## **Initial process** ##

### Step 1: Clone Repository ###

     Git clone <bitbucket link>

### Step 2: Install dependencies ###
       
     npm install
     
### Step 3: Run project ###

     npm start
###
Execution endpoints (in local server): ###

1.GET  http://localhost:7000/genres  -----> to get all genres stored in the database.

2.GET  http://localhost:7000/books  -----> to get all books stored in the database.

3.GET  http://localhost:7000/books/:_id  -----> to get book by it's id.

4.POST  http://localhost:7000/genres/addgenre  -----> to post a new genre into the database.

5.POST  http://localhost:7000/genres/addbook  -----> to post a new book into the database.